﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class PlateauJeu
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PlateauJeu))
        Me.pnl_plateau = New System.Windows.Forms.Panel()
        Me.pnl_playerround = New System.Windows.Forms.Panel()
        Me.lbl_nextplayer = New System.Windows.Forms.Label()
        Me.lbl_nextround = New System.Windows.Forms.Label()
        Me.lbl_currentplayer = New System.Windows.Forms.Label()
        Me.lbl_currentround = New System.Windows.Forms.Label()
        Me.pnl_playerscores = New System.Windows.Forms.Panel()
        Me.lbl_player4 = New System.Windows.Forms.Label()
        Me.lbl_player3 = New System.Windows.Forms.Label()
        Me.lbl_player2 = New System.Windows.Forms.Label()
        Me.lbl_player1 = New System.Windows.Forms.Label()
        Me.pnl_main = New System.Windows.Forms.Panel()
        Me.pic_main6 = New System.Windows.Forms.PictureBox()
        Me.pic_main5 = New System.Windows.Forms.PictureBox()
        Me.pic_main4 = New System.Windows.Forms.PictureBox()
        Me.pic_main3 = New System.Windows.Forms.PictureBox()
        Me.pic_main2 = New System.Windows.Forms.PictureBox()
        Me.pic_main1 = New System.Windows.Forms.PictureBox()
        Me.lbl_pioche = New System.Windows.Forms.Label()
        Me.pic_tuilepioche = New System.Windows.Forms.PictureBox()
        Me.pic_pioche = New System.Windows.Forms.PictureBox()
        Me.btn_giveup = New System.Windows.Forms.Button()
        Me.btn_valid = New System.Windows.Forms.Button()
        Me.lbl_round = New System.Windows.Forms.Label()
        Me.PicMenuPlateau = New System.Windows.Forms.PictureBox()
        Me.pnl_playerround.SuspendLayout()
        Me.pnl_playerscores.SuspendLayout()
        Me.pnl_main.SuspendLayout()
        CType(Me.pic_main6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_main5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_main4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_main3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_main2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_main1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_tuilepioche, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pic_pioche, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PicMenuPlateau, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnl_plateau
        '
        Me.pnl_plateau.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.pnl_plateau.AutoSize = True
        Me.pnl_plateau.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pnl_plateau.BackColor = System.Drawing.Color.Transparent
        Me.pnl_plateau.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl_plateau.ForeColor = System.Drawing.SystemColors.ControlText
        Me.pnl_plateau.Location = New System.Drawing.Point(375, 1)
        Me.pnl_plateau.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.pnl_plateau.Name = "pnl_plateau"
        Me.pnl_plateau.Size = New System.Drawing.Size(2, 2)
        Me.pnl_plateau.TabIndex = 0
        '
        'pnl_playerround
        '
        Me.pnl_playerround.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.pnl_playerround.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl_playerround.Controls.Add(Me.lbl_nextplayer)
        Me.pnl_playerround.Controls.Add(Me.lbl_nextround)
        Me.pnl_playerround.Controls.Add(Me.lbl_currentplayer)
        Me.pnl_playerround.Controls.Add(Me.lbl_currentround)
        Me.pnl_playerround.Location = New System.Drawing.Point(44, 85)
        Me.pnl_playerround.Margin = New System.Windows.Forms.Padding(0, 0, 15, 0)
        Me.pnl_playerround.Name = "pnl_playerround"
        Me.pnl_playerround.Size = New System.Drawing.Size(205, 125)
        Me.pnl_playerround.TabIndex = 2
        '
        'lbl_nextplayer
        '
        Me.lbl_nextplayer.AutoSize = True
        Me.lbl_nextplayer.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nextplayer.Location = New System.Drawing.Point(26, 90)
        Me.lbl_nextplayer.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_nextplayer.Name = "lbl_nextplayer"
        Me.lbl_nextplayer.Size = New System.Drawing.Size(100, 17)
        Me.lbl_nextplayer.TabIndex = 9
        Me.lbl_nextplayer.Text = "Joueur : Alexis"
        '
        'lbl_nextround
        '
        Me.lbl_nextround.AutoSize = True
        Me.lbl_nextround.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_nextround.Location = New System.Drawing.Point(10, 62)
        Me.lbl_nextround.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_nextround.Name = "lbl_nextround"
        Me.lbl_nextround.Size = New System.Drawing.Size(64, 18)
        Me.lbl_nextround.TabIndex = 8
        Me.lbl_nextround.Text = "Suivant :"
        '
        'lbl_currentplayer
        '
        Me.lbl_currentplayer.AutoSize = True
        Me.lbl_currentplayer.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_currentplayer.Location = New System.Drawing.Point(26, 38)
        Me.lbl_currentplayer.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_currentplayer.Name = "lbl_currentplayer"
        Me.lbl_currentplayer.Size = New System.Drawing.Size(100, 17)
        Me.lbl_currentplayer.TabIndex = 8
        Me.lbl_currentplayer.Text = "Joueur : Alexis"
        '
        'lbl_currentround
        '
        Me.lbl_currentround.AutoSize = True
        Me.lbl_currentround.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_currentround.Location = New System.Drawing.Point(10, 9)
        Me.lbl_currentround.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_currentround.Name = "lbl_currentround"
        Me.lbl_currentround.Size = New System.Drawing.Size(109, 18)
        Me.lbl_currentround.TabIndex = 0
        Me.lbl_currentround.Text = "Tour en cours :"
        '
        'pnl_playerscores
        '
        Me.pnl_playerscores.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.pnl_playerscores.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl_playerscores.Controls.Add(Me.lbl_player4)
        Me.pnl_playerscores.Controls.Add(Me.lbl_player3)
        Me.pnl_playerscores.Controls.Add(Me.lbl_player2)
        Me.pnl_playerscores.Controls.Add(Me.lbl_player1)
        Me.pnl_playerscores.Location = New System.Drawing.Point(44, 240)
        Me.pnl_playerscores.Margin = New System.Windows.Forms.Padding(0, 0, 15, 0)
        Me.pnl_playerscores.Name = "pnl_playerscores"
        Me.pnl_playerscores.Size = New System.Drawing.Size(206, 130)
        Me.pnl_playerscores.TabIndex = 3
        '
        'lbl_player4
        '
        Me.lbl_player4.AutoSize = True
        Me.lbl_player4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_player4.Location = New System.Drawing.Point(10, 91)
        Me.lbl_player4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_player4.Name = "lbl_player4"
        Me.lbl_player4.Size = New System.Drawing.Size(106, 20)
        Me.lbl_player4.TabIndex = 12
        Me.lbl_player4.Text = "Alexis : 30 pts"
        Me.lbl_player4.Visible = False
        '
        'lbl_player3
        '
        Me.lbl_player3.AutoSize = True
        Me.lbl_player3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_player3.Location = New System.Drawing.Point(10, 64)
        Me.lbl_player3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_player3.Name = "lbl_player3"
        Me.lbl_player3.Size = New System.Drawing.Size(106, 20)
        Me.lbl_player3.TabIndex = 11
        Me.lbl_player3.Text = "Alexis : 30 pts"
        Me.lbl_player3.Visible = False
        '
        'lbl_player2
        '
        Me.lbl_player2.AutoSize = True
        Me.lbl_player2.BackColor = System.Drawing.Color.Transparent
        Me.lbl_player2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_player2.Location = New System.Drawing.Point(10, 37)
        Me.lbl_player2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_player2.Name = "lbl_player2"
        Me.lbl_player2.Size = New System.Drawing.Size(106, 20)
        Me.lbl_player2.TabIndex = 10
        Me.lbl_player2.Text = "Alexis : 30 pts"
        Me.lbl_player2.Visible = False
        '
        'lbl_player1
        '
        Me.lbl_player1.AutoSize = True
        Me.lbl_player1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_player1.Location = New System.Drawing.Point(10, 11)
        Me.lbl_player1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbl_player1.Name = "lbl_player1"
        Me.lbl_player1.Size = New System.Drawing.Size(106, 20)
        Me.lbl_player1.TabIndex = 9
        Me.lbl_player1.Text = "Alexis : 30 pts"
        Me.lbl_player1.Visible = False
        '
        'pnl_main
        '
        Me.pnl_main.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.pnl_main.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.pnl_main.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pnl_main.Controls.Add(Me.pic_main6)
        Me.pnl_main.Controls.Add(Me.pic_main5)
        Me.pnl_main.Controls.Add(Me.pic_main4)
        Me.pnl_main.Controls.Add(Me.pic_main3)
        Me.pnl_main.Controls.Add(Me.pic_main2)
        Me.pnl_main.Controls.Add(Me.pic_main1)
        Me.pnl_main.Location = New System.Drawing.Point(51, 489)
        Me.pnl_main.Margin = New System.Windows.Forms.Padding(0, 0, 15, 0)
        Me.pnl_main.Name = "pnl_main"
        Me.pnl_main.Size = New System.Drawing.Size(192, 126)
        Me.pnl_main.TabIndex = 5
        '
        'pic_main6
        '
        Me.pic_main6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pic_main6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_main6.Location = New System.Drawing.Point(128, 64)
        Me.pic_main6.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.pic_main6.Name = "pic_main6"
        Me.pic_main6.Size = New System.Drawing.Size(57, 52)
        Me.pic_main6.TabIndex = 9
        Me.pic_main6.TabStop = False
        '
        'pic_main5
        '
        Me.pic_main5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pic_main5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_main5.Location = New System.Drawing.Point(68, 64)
        Me.pic_main5.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.pic_main5.Name = "pic_main5"
        Me.pic_main5.Size = New System.Drawing.Size(57, 52)
        Me.pic_main5.TabIndex = 9
        Me.pic_main5.TabStop = False
        '
        'pic_main4
        '
        Me.pic_main4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pic_main4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_main4.Location = New System.Drawing.Point(6, 64)
        Me.pic_main4.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.pic_main4.Name = "pic_main4"
        Me.pic_main4.Size = New System.Drawing.Size(57, 52)
        Me.pic_main4.TabIndex = 9
        Me.pic_main4.TabStop = False
        '
        'pic_main3
        '
        Me.pic_main3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pic_main3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_main3.Location = New System.Drawing.Point(128, 7)
        Me.pic_main3.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.pic_main3.Name = "pic_main3"
        Me.pic_main3.Size = New System.Drawing.Size(57, 52)
        Me.pic_main3.TabIndex = 9
        Me.pic_main3.TabStop = False
        '
        'pic_main2
        '
        Me.pic_main2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pic_main2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_main2.Location = New System.Drawing.Point(67, 7)
        Me.pic_main2.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.pic_main2.Name = "pic_main2"
        Me.pic_main2.Size = New System.Drawing.Size(57, 52)
        Me.pic_main2.TabIndex = 9
        Me.pic_main2.TabStop = False
        '
        'pic_main1
        '
        Me.pic_main1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pic_main1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.pic_main1.Location = New System.Drawing.Point(6, 7)
        Me.pic_main1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.pic_main1.Name = "pic_main1"
        Me.pic_main1.Size = New System.Drawing.Size(57, 52)
        Me.pic_main1.TabIndex = 8
        Me.pic_main1.TabStop = False
        '
        'lbl_pioche
        '
        Me.lbl_pioche.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_pioche.BackColor = System.Drawing.Color.Transparent
        Me.lbl_pioche.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_pioche.ImageAlign = System.Drawing.ContentAlignment.TopLeft
        Me.lbl_pioche.Location = New System.Drawing.Point(123, 413)
        Me.lbl_pioche.Margin = New System.Windows.Forms.Padding(0, 0, 15, 0)
        Me.lbl_pioche.Name = "lbl_pioche"
        Me.lbl_pioche.Size = New System.Drawing.Size(32, 19)
        Me.lbl_pioche.TabIndex = 9
        Me.lbl_pioche.Text = "9"
        Me.lbl_pioche.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pic_tuilepioche
        '
        Me.pic_tuilepioche.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.pic_tuilepioche.BackgroundImage = CType(resources.GetObject("pic_tuilepioche.BackgroundImage"), System.Drawing.Image)
        Me.pic_tuilepioche.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pic_tuilepioche.Location = New System.Drawing.Point(153, 425)
        Me.pic_tuilepioche.Margin = New System.Windows.Forms.Padding(0, 0, 15, 0)
        Me.pic_tuilepioche.Name = "pic_tuilepioche"
        Me.pic_tuilepioche.Size = New System.Drawing.Size(15, 17)
        Me.pic_tuilepioche.TabIndex = 11
        Me.pic_tuilepioche.TabStop = False
        '
        'pic_pioche
        '
        Me.pic_pioche.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.pic_pioche.BackColor = System.Drawing.Color.Transparent
        Me.pic_pioche.BackgroundImage = Global.Qwirkle.My.Resources.Resources.ImgPioche2
        Me.pic_pioche.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.pic_pioche.Location = New System.Drawing.Point(108, 393)
        Me.pic_pioche.Margin = New System.Windows.Forms.Padding(0, 0, 15, 0)
        Me.pic_pioche.Name = "pic_pioche"
        Me.pic_pioche.Size = New System.Drawing.Size(60, 60)
        Me.pic_pioche.TabIndex = 8
        Me.pic_pioche.TabStop = False
        '
        'btn_giveup
        '
        Me.btn_giveup.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.btn_giveup.BackColor = System.Drawing.Color.Transparent
        Me.btn_giveup.BackgroundImage = Global.Qwirkle.My.Resources.Resources.BtnAbandonner
        Me.btn_giveup.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn_giveup.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_giveup.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btn_giveup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Firebrick
        Me.btn_giveup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.IndianRed
        Me.btn_giveup.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_giveup.ForeColor = System.Drawing.Color.DarkRed
        Me.btn_giveup.Location = New System.Drawing.Point(82, 684)
        Me.btn_giveup.Margin = New System.Windows.Forms.Padding(0, 0, 15, 0)
        Me.btn_giveup.Name = "btn_giveup"
        Me.btn_giveup.Size = New System.Drawing.Size(128, 27)
        Me.btn_giveup.TabIndex = 7
        Me.btn_giveup.Text = "Abandonner"
        Me.btn_giveup.UseVisualStyleBackColor = False
        '
        'btn_valid
        '
        Me.btn_valid.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.btn_valid.AutoSize = True
        Me.btn_valid.BackColor = System.Drawing.Color.Transparent
        Me.btn_valid.BackgroundImage = Global.Qwirkle.My.Resources.Resources.BtnValider
        Me.btn_valid.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btn_valid.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btn_valid.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.btn_valid.FlatAppearance.MouseDownBackColor = System.Drawing.Color.ForestGreen
        Me.btn_valid.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LimeGreen
        Me.btn_valid.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.2!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_valid.ForeColor = System.Drawing.Color.LimeGreen
        Me.btn_valid.Location = New System.Drawing.Point(82, 652)
        Me.btn_valid.Margin = New System.Windows.Forms.Padding(0, 0, 15, 0)
        Me.btn_valid.Name = "btn_valid"
        Me.btn_valid.Size = New System.Drawing.Size(128, 27)
        Me.btn_valid.TabIndex = 6
        Me.btn_valid.Text = "Changer tuiles"
        Me.btn_valid.UseVisualStyleBackColor = False
        '
        'lbl_round
        '
        Me.lbl_round.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_round.AutoSize = True
        Me.lbl_round.BackColor = System.Drawing.Color.Transparent
        Me.lbl_round.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_round.ForeColor = System.Drawing.Color.Black
        Me.lbl_round.Image = Global.Qwirkle.My.Resources.Resources.FontMenuPlateau1
        Me.lbl_round.Location = New System.Drawing.Point(82, 23)
        Me.lbl_round.Margin = New System.Windows.Forms.Padding(0, 0, 15, 0)
        Me.lbl_round.Name = "lbl_round"
        Me.lbl_round.Size = New System.Drawing.Size(138, 37)
        Me.lbl_round.TabIndex = 1
        Me.lbl_round.Text = "Tour n°1"
        '
        'PicMenuPlateau
        '
        Me.PicMenuPlateau.BackColor = System.Drawing.Color.Transparent
        Me.PicMenuPlateau.BackgroundImage = Global.Qwirkle.My.Resources.Resources.PicMenuPlateau
        Me.PicMenuPlateau.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PicMenuPlateau.Location = New System.Drawing.Point(21, 10)
        Me.PicMenuPlateau.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.PicMenuPlateau.Name = "PicMenuPlateau"
        Me.PicMenuPlateau.Size = New System.Drawing.Size(251, 748)
        Me.PicMenuPlateau.TabIndex = 12
        Me.PicMenuPlateau.TabStop = False
        '
        'PlateauJeu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1186, 717)
        Me.Controls.Add(Me.pic_tuilepioche)
        Me.Controls.Add(Me.lbl_pioche)
        Me.Controls.Add(Me.pic_pioche)
        Me.Controls.Add(Me.btn_giveup)
        Me.Controls.Add(Me.btn_valid)
        Me.Controls.Add(Me.pnl_main)
        Me.Controls.Add(Me.pnl_playerscores)
        Me.Controls.Add(Me.pnl_playerround)
        Me.Controls.Add(Me.lbl_round)
        Me.Controls.Add(Me.pnl_plateau)
        Me.Controls.Add(Me.PicMenuPlateau)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "PlateauJeu"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Qwirkle"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.pnl_playerround.ResumeLayout(False)
        Me.pnl_playerround.PerformLayout()
        Me.pnl_playerscores.ResumeLayout(False)
        Me.pnl_playerscores.PerformLayout()
        Me.pnl_main.ResumeLayout(False)
        CType(Me.pic_main6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_main5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_main4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_main3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_main2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_main1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_tuilepioche, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pic_pioche, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PicMenuPlateau, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents pnl_plateau As Panel
    Friend WithEvents pic_main5 As PictureBox
    Friend WithEvents pic_main4 As PictureBox
    Friend WithEvents pic_main3 As PictureBox
    Friend WithEvents pic_main2 As PictureBox
    Friend WithEvents pic_main1 As PictureBox
    Friend WithEvents lbl_nextplayer As Label
    Friend WithEvents lbl_nextround As Label
    Friend WithEvents lbl_currentplayer As Label
    Friend WithEvents lbl_currentround As Label
    Friend WithEvents lbl_player4 As Label
    Friend WithEvents lbl_player3 As Label
    Friend WithEvents lbl_player2 As Label
    Friend WithEvents lbl_player1 As Label
    Friend WithEvents pic_main6 As PictureBox
    Friend WithEvents lbl_round As Label
    Friend WithEvents pnl_playerround As Panel
    Friend WithEvents pnl_playerscores As Panel
    Friend WithEvents pnl_main As Panel
    Friend WithEvents btn_valid As Button
    Friend WithEvents btn_giveup As Button
    Friend WithEvents lbl_pioche As Label
    Friend WithEvents pic_tuilepioche As PictureBox
    Friend WithEvents pic_pioche As PictureBox
    Friend WithEvents PicMenuPlateau As PictureBox
End Class
